/*
 *
 * @author Matteo Cappella
 * @author Simone Passaretti
 *
*/
package ia_kr;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;


//BUTTON EDITOR CLASS
class ButtonEditor extends DefaultCellEditor {
   protected JButton btn;
   private String lbl;
   private Boolean clicked;
   int row_selected;
   String product;
   String price;
   String quantity;
   
   private String action = "";
   
   public ButtonEditor(JTextField txt, String action) {
    super(txt);
    
    this.action = action;
    
    btn=new JButton();
    btn.setOpaque(true);
    
    //WHEN BUTTON IS CLICKED
    btn.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {  
        fireEditingStopped();
      }
    });
  }
   
   //OVERRIDE A COUPLE OF METHODS
   @Override
  public Component getTableCellEditorComponent(JTable table, Object obj,
      boolean selected, int row, int col) {

     //SET TEXT TO BUTTON,SET CLICKED TO TRUE,THEN RETURN THE BTN OBJECT
     row_selected =table.getSelectedRow();
     product= (String) table.getValueAt(row_selected, 0);
     price = (String) table.getValueAt(row_selected, 1);
     quantity = (String) table.getValueAt(row_selected, 2);
     lbl=(obj==null) ? "":obj.toString();
     btn.setText(lbl);
     clicked=true;
     
    return btn;
  }
   
  //IF BUTTON CELL VALUE CHNAGES,IF CLICKED THAT IS
   @Override
    public Object getCellEditorValue() {
        Cart cart = Cart.getInstance();
        Map<String, Float> cart_content = cart.cart_content;
        Map<String, Integer> cart_content_quantity = cart.cart_content_quantity;
        
        if(clicked){   
            if(action.equals("put")){
                try {
                    int quantity_int = Integer.parseInt(quantity);
                    String price_without_euro = price.replace(" €", "");
                    Float price_float = Float.valueOf(price_without_euro);
                    BigDecimal bd = new BigDecimal(Float.toString(price_float));
                    bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                     
                    cart_content.put(product, bd.floatValue());
                    cart_content_quantity.put(product, quantity_int);
                    cart.initRecap();
                    JOptionPane.showMessageDialog(null,"The product " + product + " (Price: "+ price+ ") \n"
                                                      + "as just been inserted into the cart" );
                  
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Error! You have to insert a number in quantity label!","Error" ,JOptionPane.ERROR_MESSAGE);
                }
                
            } else if (action.equals("remove")) {
                cart_content.remove(product);
                cart_content_quantity.remove(product);
                cart.initRecap();
                
                JOptionPane.showMessageDialog(null,"The product " + product + " (Price: "+ price+ ") \n"
                                                      + "as just been removed into the cart" );
                if(cart_content.size() == 0){
                    cart.cart_frame.setVisible(false);
                }
            }
            
                
        }
        //SET IT TO FALSE NOW THAT ITS CLICKED
        clicked = false;
        return new String(lbl);
    }
  
   @Override
  public boolean stopCellEditing() {

      //SET CLICKED TO FALSE FIRST
      clicked=false;
    return super.stopCellEditing();
  }
   
   @Override
  protected void fireEditingStopped() {
    // TODO Auto-generated method stub
    super.fireEditingStopped();
  }
}