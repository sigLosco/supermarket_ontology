/*
 *
 * @author Matteo Cappella
 * @author Simone Passaretti
 *
*/
package ia_kr;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import org.apache.jena.rdf.model.Model;
import org.json.simple.parser.ParseException;


public class View extends JFrame{
    /*
    * ATTRIBUTE
    */
    JTabbedPane Supermarket_products = new JTabbedPane(JTabbedPane.TOP);
    float min_budget = 50.0f;
    float max_budget = 100.0f;
    float budget = 0.0f;
    
    public View() throws ParseException {
        //settings of frame
        super("Super Market Products");
        //setSize(500, 800); //frame size 
        setLayout(new GridBagLayout()); //frame layout
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createAndShowGUI();
        pack();
        setLocationRelativeTo(null);
        setVisible(true); // Display the window.   
    }
    
    public void createAndShowGUI() throws ParseException {   
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx= 1;
        gbc.gridy = 0;
        
        Random rand = new Random();
        budget = rand.nextFloat() * (max_budget - min_budget) + min_budget;
        budget = (float) (Math.round(budget * 100.0)/100.0);
        add(new JLabel("Budget: " + budget + " €"), gbc);
        
        gbc.gridx = 2;
        gbc.gridy = 0;
        
        //add cart button
        JPanel cart_button = new JPanel();
        cart_button.add(new JButton(new AbstractAction("Cart") {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cart cart = Cart.getInstance();
                Map <String, Float> cart_content = cart.cart_content;
                
                if(cart_content.size() == 0){
                     JOptionPane.showMessageDialog(null, "Empty Cart! Put at least an element inside it","Error",  JOptionPane.ERROR_MESSAGE);
                } else {
                    cart.budget = budget;
                    cart.initRecap();
                    cart.set_visible_cart();
                }
                
               
            }
        }));
        add(cart_button, gbc);
        
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        //gbc.fill = GridBagConstraints.HORIZONTAL;
        createTabs("Supermarket_products", Supermarket_products);
        //getContentPane().add(Supermarket_products, BorderLayout.CENTER);
        add(Supermarket_products, gbc);
        
        Recipe list_recipe = new Recipe();
        JPanel recipe = list_recipe.create_list_recipe();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbc.insets = new Insets(0,0,0,100);

        add(recipe, gbc);
        
        
    }

    public JTabbedPane makeTab() {
        JTabbedPane p = new JTabbedPane();
        return p;
    }

    public void createTabs (String name, JTabbedPane tabbedPane) {
        RDF_model rdf_model = new RDF_model();
        Model model = rdf_model.getModel(rdf_model.inputFileName);
        ArrayList<String> classOfSupermarket = rdf_model.get_subClasses(model, name);

        if(classOfSupermarket.size() > 0){
            for(int i = 0; i< classOfSupermarket.size(); i++) {
                JTabbedPane subclass = makeTab();
                createTabs(classOfSupermarket.get(i), subclass);
                tabbedPane.add(classOfSupermarket.get(i), subclass);
            }
        } else {
            Map<String,Float> instance_and_price = rdf_model.get_InstanciesAndPrice(model,name);
            //Set up the editor for the sport cells.
            
            String [] column = {"Product","Price","Quantity", "Put in the cart"};
            Object[][] arr = new Object[instance_and_price.size()][4];

            Set entries = instance_and_price.entrySet();
            Iterator entriesIterator = entries.iterator();

            int i = 0;
            
            while(entriesIterator.hasNext()){

                Map.Entry mapping = (Map.Entry) entriesIterator.next();

                arr[i][0] = mapping.getKey();
                arr[i][1] = mapping.getValue() + " €";
                arr[i][2] = "1";
                arr[i][3] = "Insert";

                i++;
            }


            JTable table = new JTable(arr,column){
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    switch (column) {
                        case 0:
                            return false;
                        case 1:
                            return false;
                        case 2:
                            return true;
                        case 3: 
                            return true;
                        default:
                            return false;
                     }
                }
            };

            table.getColumnModel().getColumn(3).setCellRenderer(new ButtonRenderer());
            table.getColumnModel().getColumn(3).setCellEditor(new ButtonEditor(new JTextField(),"put"));               
            table.setRowSelectionAllowed(true);
            table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

            tabbedPane.add(new JScrollPane(table), "Select Products");


        }
    }
}
