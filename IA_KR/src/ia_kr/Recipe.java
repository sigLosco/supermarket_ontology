/*
 *
 * @author Matteo Cappella
 * @author Simone Passaretti
 */
package ia_kr;

import java.awt.Color;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Recipe {
   
   //get relative path of json configuration file 
   URL url = getClass().getResource("list_of_product_to_use.json");
   String inputFile = url.getPath();
   JSONObject list_recipe = null;
   int recipe_to_get = 0;
   int n_rnd_products_to_get = 3;
   Map<String,Integer> rnd_products_to_get = new HashMap();
   int max_n_people= 5;
   int n_people_recipe=0;
   
   public Recipe() throws org.json.simple.parser.ParseException {
       JSONParser parser = new JSONParser();
        try {
           list_recipe =(JSONObject) parser.parse(new FileReader(inputFile));
           JSONArray recipes = (JSONArray) list_recipe.get("recipes");
           Random rand = new Random();
           recipe_to_get = Math.round(rand.nextFloat() * (recipes.size()-1));
           n_people_recipe= Math.round(rand.nextFloat()*(max_n_people-1)+1); //extract number between 1 to 4
           get_rnd_products();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
   }
   
   public JPanel create_list_recipe () {
        JPanel recipe = new JPanel();
        recipe.setPreferredSize(new Dimension(400, 600));
        recipe.setBackground(Color.white);
        recipe.setBorder(BorderFactory.createLineBorder(Color.gray, 4));
        JLabel title = new JLabel("Shopping list: "+ get_recipe_name() +" for " + n_people_recipe + " people");  
        recipe.add(title);
        JList list = new JList(get_array_from_json()); //data has type Object[]
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);
        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(370, 550));
        recipe.add(listScroller);
        return recipe;
   }
   
   public String [] get_array_from_json() {
       ArrayList<String> arraylist_products = new ArrayList();
       
       JSONArray recipes = (JSONArray) list_recipe.get("recipes");
       
       JSONArray rnd_products_array = (JSONArray) list_recipe.get("random_products");
       
       JSONObject obj = (JSONObject) recipes.get(recipe_to_get);
       JSONArray products = (JSONArray) obj.get("products_to_buy");
       Map <String, Integer> product_and_n_people = get_recipe_product_n_people();       
       for(int i=0; i< products.size(); i++) {
           JSONObject app = (JSONObject) products.get(i);
           String n_people= (product_and_n_people.get((String) app.get("instance"))).toString();
           String instance_and_n_people = (String) app.get("instance") + " x " + n_people;
           arraylist_products.add(instance_and_n_people);
        
       }
       
       arraylist_products.add("\n");
       arraylist_products.add("Other products");
       arraylist_products.add("\n");
       
       for(Map.Entry entry : rnd_products_to_get.entrySet()) {         
           String class_and_n_people = entry.getKey() + " x " + get_qnt((Integer) entry.getValue());
           arraylist_products.add(class_and_n_people);
       }
       
       
       
       //Cart.array_recipe = arraylist_products.toArray(new String[arraylist_products.size()]);
       return arraylist_products.toArray(new String[arraylist_products.size()]);
   }
   
    public String get_recipe_name() {
        JSONArray obj = (JSONArray) list_recipe.get("recipes");
        JSONObject recipe = (JSONObject) obj.get(recipe_to_get);
       return (String) recipe.get("name");
    }
   
    public Map<String, Integer> get_recipe_product_n_people() {
        Map<String, Integer> product_and_n_people = new HashMap();
        Map<String, Integer> class_and_n_people = new HashMap();
        
        JSONArray obj = (JSONArray) list_recipe.get("recipes");
        
        JSONObject recipe = (JSONObject) obj.get(recipe_to_get);       
        JSONArray products = (JSONArray) recipe.get("products_to_buy");
        
        for(int i=0; i< products.size(); i++) {
            JSONObject app = (JSONObject) products.get(i);           
           
            product_and_n_people.put((String) app.get("instance"), get_qnt(Integer.parseInt((String) app.get("n_people"))));
        }
        
        for(Map.Entry entry : rnd_products_to_get.entrySet()) {
            class_and_n_people.put((String) entry.getKey(),get_qnt((Integer) entry.getValue()));
        }
       
       Cart.rnd_prodcuts = class_and_n_people;
       Cart.map_recipe = product_and_n_people;
       return product_and_n_people;
    }
    
    public int get_qnt(int n_people){
        int n = (int) Math.floor((double)(n_people_recipe / n_people));
        if((n_people_recipe % n_people) !=0){
            n++;
        } 
        
        return n;
    }
    
    public void get_rnd_products(){
        
        Random rng = new Random(); // Ideally just create one instance globally
        // Note: use LinkedHashSet to maintain insertion order
        Set<Integer> generated = new LinkedHashSet();
        JSONArray rnd_products_array = (JSONArray) list_recipe.get("random_products");
        
        while (generated.size() < n_rnd_products_to_get)
        {
            Integer next = rng.nextInt(rnd_products_array.size()-1) + 1;
            // As we're adding to a set, this will automatically do a containment check
            generated.add(next);
        }
        
        Iterator<Integer> itr = generated.iterator();
        
        JSONArray rnd_products = (JSONArray) list_recipe.get("random_products");
        
        while(itr.hasNext()){
            int a = itr.next();
            System.out.println(a);
            
            JSONObject rnd_product_to_get = (JSONObject) rnd_products.get(a);
            System.out.println(rnd_product_to_get.get("class").toString());
            rnd_products_to_get.put(rnd_product_to_get.get("class").toString(), Integer.parseInt(rnd_product_to_get.get("n_people").toString()));
        }
        
        
    }
}
