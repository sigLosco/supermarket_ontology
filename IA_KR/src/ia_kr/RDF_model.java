/*
 *
 * @author Matteo Cappella
 * @author Simone Passaretti
 *
*/
package ia_kr;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;


public class RDF_model {
    
    //get relative path of ontology resource
    URL url = getClass().getResource("supermarket_ontology.rdf");
    String inputFileName = url.getPath();
    
    //create a model from a path
    public static Model getModel(String path){
        
        // create an empty model
        Model model = ModelFactory.createDefaultModel();
        // use the FileManager to find the input file
        InputStream in = FileManager.get().open( path );
        if (in == null) {
            throw new IllegalArgumentException("File: " + path + " not found");
        }
       model.read(in, null);
       return model;
    }
    
    //query that return all subclasses
    public static ArrayList<String> get_subClasses(Model model, String name) {
        
        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                             "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                             "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                             "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                             "PREFIX supermarket: <http://www.semanticweb.org/matteo/ontologies/2017/5/supermarket#>\n" +
                             "SELECT ?subject (strafter(str(?subject), \"#\") AS ?class_name)\n" +
                             "WHERE { ?subject rdfs:subClassOf supermarket:" + name + " }";
        Query query = QueryFactory.create(queryString);
        QueryExecution query_exec = QueryExecutionFactory.create(query, model);
        
        try{
            ResultSet results = query_exec.execSelect();
            ArrayList<String> class_name_array = new ArrayList<String>();
            while (results.hasNext()) {
                QuerySolution solution = results.nextSolution();
                //System.out.println(solution);
                Literal class_name = solution.getLiteral("class_name");
                class_name_array.add(class_name.getString());
            }
            return class_name_array;
        } finally {
            query_exec.close();
        }     
    }
    
    public static ArrayList<String> get_instances_from_class(Model model, String name) {
        
        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                             "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                             "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                             "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                             "PREFIX supermarket: <http://www.semanticweb.org/matteo/ontologies/2017/5/supermarket#>\n" +
                             "SELECT (strafter(str(?instance), \"#\") AS ?instance_name)\n" +
                             "WHERE { ?instance rdf:type supermarket:" + name + " }";
        Query query = QueryFactory.create(queryString);
        QueryExecution query_exec = QueryExecutionFactory.create(query, model);
        
        try{
            ResultSet results = query_exec.execSelect();
            ArrayList<String> instances_from_class = new ArrayList<String>();
            while (results.hasNext()) {
                QuerySolution solution = results.nextSolution();
                System.out.println(solution);
                Literal instance = solution.getLiteral("instance_name");
                instances_from_class.add(instance.getString());
            }
            return instances_from_class;
        } finally {
            query_exec.close();
        }     
    }
    
   //query that return all instances and price
    public static Map<String, Float> get_InstanciesAndPrice(Model model, String name) {
        
        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                             "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                             "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                             "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                             "PREFIX supermarket: <http://www.semanticweb.org/matteo/ontologies/2017/5/supermarket#>\n" +
                             "SELECT DISTINCT ?obj (strafter(str(?obj), \"#\") AS ?instance) ?price\n" +
                             "WHERE { ?obj rdf:type supermarket:"+ name +".\n" +
                             "?obj supermarket:price  ?price}";
        Query query = QueryFactory.create(queryString);
        QueryExecution query_exec = QueryExecutionFactory.create(query, model);
        
        try{
            ResultSet results = query_exec.execSelect();
            Map<String, Float> instance_and_price = new HashMap<String,Float>();
            while (results.hasNext()) {
                QuerySolution solution = results.nextSolution();
                //System.out.println(solution);
                Literal instance_name= solution.getLiteral("instance");
                String instance_name_string = instance_name.getString();
                Literal instance_price = solution.getLiteral("price");
                Float instance_price_float = instance_price.getFloat();
                instance_and_price.put(instance_name_string, instance_price_float);
            }
            /*
            for(Map.Entry entry : instance_and_price.entrySet()) {
                System.out.println(entry.getKey() + ", " + entry.getValue());
            }
            */
            return instance_and_price;
        } finally {
            query_exec.close();
        }     
    }
}
