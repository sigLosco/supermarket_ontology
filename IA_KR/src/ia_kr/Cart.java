/*
 *
 * @author Matteo Cappella
 * @author Simone Passaretti
 *
*/
package ia_kr;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import org.apache.jena.rdf.model.Model;


public class Cart{
    
    /**
    *
    *   Attributi
    * 
    */
        
    private static Cart cart = null;
    static JFrame cart_frame = null;
    private static JPanel cart_panel = null;
    static Map<String, Float> cart_content = null;
    static Map<String, Integer> cart_content_quantity= null;
    private static JTable recap = null;
    private static JTextField text_field = null;
    private static JButton check_button = null;
    //static String [] array_recipe;
    static Map<String, Integer> map_recipe= null;
    static Map<String, Integer> rnd_prodcuts= null;
    static float budget = 0;
    
    
    protected Cart() {}
    
    public static Cart getInstance() {
        if(cart == null) {
            cart = new Cart();
            cart_frame = new JFrame("Cart");
            cart_frame.setLayout(new BorderLayout());
            cart_frame.setSize(500,300);
            cart_frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            cart_frame.setUndecorated(true);
            cart_panel = new JPanel(new GridBagLayout()); 
            cart_frame.add(cart_panel,BorderLayout.CENTER);
            cart_content = new HashMap();
            cart_content_quantity = new HashMap();
            text_field = new JTextField(6);
            check_button = new JButton("Check");
            
            check_button.addActionListener(
                new ActionListener() { 
                    public void actionPerformed(ActionEvent e) { 
                        check_amount(budget);
                    } 
                } 
            );
        }
       
       return cart;
    }
    
    public static void initRecap() {
     
        cart_panel.removeAll(); 
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        
        gbc.gridwidth = 2;
        recap = createRecap();
        JScrollPane jscroll = new JScrollPane(recap);

        cart_panel.add(jscroll,gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        cart_panel.add(new JLabel("Calculate the total cost and enter it in the box"),gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        cart_panel.add(text_field,gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        cart_panel.add(check_button,gbc);
        cart_panel.revalidate();
        cart_panel.repaint();
       
    }
    
    public static JTable createRecap() {
        String [] column = {"Product","Price","Quantity", "Remove from cart"};
        Object[][] arr = new Object[cart_content.size()][4];

        Set entries = cart_content.entrySet();
        Set entries_quantity = cart_content_quantity.entrySet();
        Iterator entriesIterator = entries.iterator();
        Iterator entriesIteratorQty = entries_quantity.iterator();
        int i = 0;
        
        while(entriesIterator.hasNext() && entriesIteratorQty.hasNext()){
            Map.Entry mapping = (Map.Entry) entriesIterator.next();
            Map.Entry mapping_quantity = (Map.Entry) entriesIteratorQty.next();
            arr[i][0] = mapping.getKey();
            Float price_x_quantity = (int) mapping_quantity.getValue() * (float) mapping.getValue(); 
            arr[i][1] = price_x_quantity.toString() + " €";
            arr[i][2] = mapping_quantity.getValue().toString();
            arr[i][3] = "Remove";
            i++;
        }
        
        JTable recap_table = new JTable(arr,column){
            @Override
            public boolean isCellEditable(int row, int column)
            {
                switch (column) {
                    case 0:
                        return false;
                    case 1:
                        return false;
                    case 2:
                        return false;
                    case 3:
                        return true;
                    default:
                        return false;
                 }
            }
        };
        
        recap_table.getColumnModel().getColumn(3).setCellRenderer(new ButtonRenderer());
        recap_table.getColumnModel().getColumn(3).setCellEditor(new ButtonEditor(new JTextField(),"remove"));               
        recap_table.setRowSelectionAllowed(true);
        recap_table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        
        return recap_table;
    }
    
    public static void set_visible_cart(){
        cart_frame.setVisible(true);
    }
    
    public static boolean compareMap() {

        System.out.println(map_recipe.size());
        map_recipe.putAll(rnd_prodcuts);
        for(Map.Entry entry : map_recipe.entrySet()) {
            Boolean found = false;
            //System.out.println(entry.getKey() + ", " + entry.getValue());
            for(Map.Entry entry_result : cart_content_quantity.entrySet()) {
                System.out.println(entry_result.getKey());
                /*System.out.println("map recipe --->" + entry_result.getKey() + ", " +entry_result.getValue());
                for(Map.Entry entry1 : cart_content_quantity.entrySet()) {
                    System.out.println("cart content quantity--->" +entry1.getKey() + ", " +entry1.getValue());
                }*/
                
                if(entry.getKey().equals(entry_result.getKey()) || checkInstances(entry.getKey().toString(), entry_result.getKey().toString())) {
                    System.out.println("found");
                    found = true;
                    if(entry.getKey().equals(entry_result.getKey())){
                        if(!entry.getValue().equals(entry_result.getValue())) {
                            System.out.println("equals");
                            return false;
                        }
                    }
               }
            }
            if (!found){
                return false;
            }
            
        }
        
        return true;
    }
    
    public static boolean checkInstances(String elem_recipe, String elem_cart) {
        RDF_model rdf_model = new RDF_model();
        Model model = rdf_model.getModel(rdf_model.inputFileName);
        ArrayList<String> instancesOfClass = rdf_model.get_instances_from_class(model, elem_recipe);
        
        if(instancesOfClass.size() > 0) {
            for(int i=0; i< instancesOfClass.size(); i++) {
                if (instancesOfClass.get(i).equals(elem_cart)) {
                    int qnt_recipe = map_recipe.get(elem_recipe);
                    int qnt_cart = cart_content_quantity.get(instancesOfClass.get(i)); 
                    if (qnt_recipe == qnt_cart ) {
                        return true;
                    } 
                }
            }
        }
        return false;
    }
       
    
    public static void check_amount(float budget) {
        String amount_string = text_field.getText();

        if(compareMap()){
            if(amount_string.equals("")){
                JOptionPane.showMessageDialog(null, "Insert a value in the box","Error", JOptionPane.ERROR_MESSAGE);
            } else {
                try {    
                    Float amount = Float.valueOf(amount_string);
                    Float total_in_cart= Float.valueOf("0.0");
                    for(Map.Entry entry : cart_content.entrySet()) {
                        for(Map.Entry entry_qty: cart_content_quantity.entrySet()) {
                            if(entry_qty.getKey().equals(entry.getKey())) {
                                total_in_cart = Float.sum(total_in_cart,  ((float)entry.getValue()* (int)entry_qty.getValue()));
                            }
                        }
                    }
                    
                    if (Float.compare(total_in_cart, budget) <= 0){
                        if(Float.compare(amount, total_in_cart) == 0) {
                            JOptionPane.showMessageDialog(null, "Correct answer! You win!", "Win!!!", JOptionPane.INFORMATION_MESSAGE);
                            System.exit(0);
                        } else {
                            JOptionPane.showMessageDialog(null, "Wrong answer! Try again...", "Error", JOptionPane.ERROR_MESSAGE);
                            text_field.setText("");
                        }
                    } else {
                       JOptionPane.showMessageDialog(null, "Budget exceeded! The total in your cart exceeds your budget.", "Error", JOptionPane.ERROR_MESSAGE); 
                    }

                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Please enter numbers only", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "OPS! It seems that the products in the cart are not the ones needed to prepare the recipe, or quantities are incorrect", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
